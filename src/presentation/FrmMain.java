package presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;

public class FrmMain extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	JDesktopPane desktop = new JDesktopPane();


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmMain frame = new FrmMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmMain() {
		setTitle("Imadec");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 679, 442);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnImagenes = new JMenu("Archivo de Entrada");
		mnImagenes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				  
			}
		});
		menuBar.add(mnImagenes);
		
		JMenuItem mntmHola = new JMenuItem("Im\u00E1genes");
		mntmHola.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Hi");
				FrmEntradaImagenes frm = new FrmEntradaImagenes();
				frm.setVisible(true);
				desktop.add(frm);                            
			    try {
			        frm.setSelected(true);                   
			    } catch (java.beans.PropertyVetoException e) {}        
			}
		});
		mnImagenes.add(mntmHola);
		
		JMenu mnNewMenu = new JMenu("Estructura");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmXml = new JMenuItem("Estableccer Datos");
		mntmXml.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Hola");
				FrmXML frm = new FrmXML();
				frm.setLocation((desktop.getWidth() - frm.getWidth())/2, 20);
				frm.setVisible(true);
				desktop.add(frm);    
				frm.toFront();
			}
		});
		mnNewMenu.add(mntmXml);
		
		JMenu mnReconocimiento = new JMenu("Reconocimiento");
		menuBar.add(mnReconocimiento);
		
		JMenuItem mntmHol = new JMenuItem("B\u00FAsqueda");
		mntmHol.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Hi");
				FrmReconocimiento frm = new FrmReconocimiento();
				frm.setVisible(true);
				desktop.add(frm);                            
			    try {
			        frm.setSelected(true);                   
			    } catch (java.beans.PropertyVetoException ex) {}    				
			}
		});
		mnReconocimiento.add(mntmHol);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(desktop);
		desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
	}
}
