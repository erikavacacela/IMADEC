package presentation;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class General {
	
	
	public static String URL_DIRECTORY_IMG ="D:/Joone/images";
	public static String URL_FILE_XML ="D:/Joone/pruebas_ex/prueba_xml_200x200_10.txt";
	public static String URL_INPUT_FILE="D:/Joone/arch_entrada_" +
										(new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()))+
										".txt" ;
	public static String URL_IMAGE_COMPARE = "D:/Joone/images/20x20/auto2.jpg";
	public static String CAPA_ENTRADA="CapaEntrada";
	public static String CAPA_OCULTA="CapaIntermedia";
	public static String CAPA_SALIDA="CapaSalida";
	public static int NUM_NEURONAS=4;
	public static int CANT_SALIDAS=10;
}
