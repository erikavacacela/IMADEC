package presentation;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JTextField;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.FlowLayout;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.border.TitledBorder;

import logic.ArchivoEntrada;

import java.awt.SystemColor;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import java.awt.Color;

public class FrmEntradaImagenes extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtLocalizacion;
	private JTextField txtDirectorio;
	private JScrollPane scrollPane;
	/**
	 * Create the frame.
	 */
	public FrmEntradaImagenes() {
		setTitle("Im\u00E1genes predefinidas");
		setClosable(true);
		setMaximizable(true);
		setBounds(100, 100, 777, 446);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		
		JLabel lblDirectorioDeImgenes = new JLabel("Directorio de Im\u00E1genes");
		panel.add(lblDirectorioDeImgenes);
		
		txtDirectorio = new JTextField();
		txtDirectorio.setColumns(40);
		txtDirectorio.setText(General.URL_DIRECTORY_IMG);
		panel.add(txtDirectorio);
		
		JButton button = new JButton("...");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				select_directory();
			}
		});
		panel.add(button);
		
		JButton btnMotrar = new JButton("Mostrar");
		btnMotrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mostrar_imagenes();
			}
		});
		panel.add(btnMotrar);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Archivo de entrada", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBackground(SystemColor.menu);
		getContentPane().add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblArchivo = new JLabel("Localizaci\u00F3n");
		lblArchivo.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(lblArchivo);
		
		txtLocalizacion = new JTextField(General.URL_INPUT_FILE);
		txtLocalizacion.setColumns(40);
		panel_1.add(txtLocalizacion);
		
		JButton btnSaveFile = new JButton("...");
		btnSaveFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save_file();
			}
		});
		panel_1.add(btnSaveFile);
		
		JButton btnGenerar = new JButton("Generar");
		btnGenerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generar_archivo();
			}
		});
		panel_1.add(btnGenerar);
		
		JPanel panel_2 = new JPanel();
		getContentPane().add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		panel_2.add(scrollPane, BorderLayout.CENTER);
		
		scrollPane.setViewportView(new TestPane(General.URL_DIRECTORY_IMG));
	}
	
	private void select_directory(){
		JFileChooser f = new JFileChooser();
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
        f.showSaveDialog(null);
        try{
        	txtDirectorio.setText(f.getSelectedFile().toString());
        	General.URL_DIRECTORY_IMG = txtDirectorio.getText();
        	System.out.println("Hola: " + General.URL_DIRECTORY_IMG);
        }catch(Exception e){
        	System.out.println(e.getMessage());
        }
	}
	
	private void mostrar_imagenes(){
		scrollPane.setViewportView(new TestPane(txtDirectorio.getText()));
	}

	private void save_file(){
		JFileChooser f = new JFileChooser();
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
        f.showSaveDialog(null);
        try{
        	txtLocalizacion.setText(f.getSelectedFile().toString());
        	General.URL_INPUT_FILE = txtLocalizacion.getText();
        	System.out.println("Hola: " + General.URL_INPUT_FILE);
        }catch(Exception e){
        	System.out.println(e.getMessage());
        }
	}
	
	private void generar_archivo(){
		ArchivoEntrada arch = new ArchivoEntrada();
		String generado = arch.generar_archivo();
		if(generado != null)
			JOptionPane.showMessageDialog(null, "El archivo de entrada ha sido generado: " + generado);
		else
			JOptionPane.showMessageDialog(null, "Ocurri� un problema al generar el archivo.");
	}
	
	public class TestPane extends JPanel {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public TestPane(String URL_DIRECTORY) {
            setLayout(new GridLayout(0, 5, 5, 5));
            File[] files = new File(URL_DIRECTORY).listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    String name = pathname.getName().toLowerCase();
                    System.out.println(name);
                    return pathname.isFile() &&
                            name.endsWith(".png") ||
                            name.endsWith(".jpg");
                }
            });
            Arrays.sort(files);
            int count = 0;
            while (count < files.length) {
                try {
                    System.out.println(count + "; " + files[count]);
                    
                    ImageIcon imageIcon = new ImageIcon(ImageIO.read(files[count])); // load the image to a imageIcon
                    Image image = imageIcon.getImage(); // transform it 
                    Image newimg = image.getScaledInstance(140, 140,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
                    imageIcon = new ImageIcon(newimg);  // transform it back
                    
                    JLabel lbl = new JLabel(imageIcon);
                    add(lbl);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                count++;
            }
        }
    }
	
}
