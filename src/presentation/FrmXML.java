package presentation;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.SwingConstants;

public class FrmXML extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtFileXML;
	private JTextField txtEntrada;
	private JTextField txtOculta;
	private JTextField txtSalida;
	private JTextField txtNeuronas;
	private JTextField txtResultados;

	/**
	 * Create the frame.
	 */
	public FrmXML() {
		setClosable(true);
		setTitle("Datos de la estructura");
		setBounds(100, 100, 609, 254);
		getContentPane().setLayout(null);
		
		JLabel lblArchivo = new JLabel("Archivo XML");
		lblArchivo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblArchivo.setHorizontalTextPosition(SwingConstants.RIGHT);
		lblArchivo.setBounds(26, 11, 79, 22);
		getContentPane().add(lblArchivo);
		
		txtFileXML = new JTextField(General.URL_FILE_XML);
		txtFileXML.setBounds(115, 12, 399, 20);
		getContentPane().add(txtFileXML);
		txtFileXML.setColumns(10);
		
		JButton btnBuscarXML = new JButton("...");
		btnBuscarXML.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscar_xml();
			}
		});
		btnBuscarXML.setBounds(524, 11, 59, 23);
		getContentPane().add(btnBuscarXML);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Nombres de las capas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 79, 573, 99);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblCapaEntrada = new JLabel("Capa Entrada");
		lblCapaEntrada.setBounds(23, 29, 89, 24);
		panel.add(lblCapaEntrada);
		
		JLabel lblCapaOculta = new JLabel("Capa Oculta");
		lblCapaOculta.setBounds(298, 30, 89, 24);
		panel.add(lblCapaOculta);
		
		JLabel lblCapaSalida = new JLabel("Capa Salida");
		lblCapaSalida.setBounds(23, 62, 89, 24);
		panel.add(lblCapaSalida);
		
		txtEntrada = new JTextField(General.CAPA_ENTRADA);
		txtEntrada.setColumns(10);
		txtEntrada.setBounds(107, 29, 161, 20);
		panel.add(txtEntrada);
		
		txtOculta = new JTextField(General.CAPA_OCULTA);
		txtOculta.setColumns(10);
		txtOculta.setBounds(382, 29, 161, 20);
		panel.add(txtOculta);
		
		txtSalida = new JTextField(General.CAPA_SALIDA);
		txtSalida.setColumns(10);
		txtSalida.setBounds(107, 60, 161, 20);
		panel.add(txtSalida);
		
		JLabel lblNoNeuronas = new JLabel("No. Neuronas");
		lblNoNeuronas.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNoNeuronas.setHorizontalTextPosition(SwingConstants.RIGHT);
		lblNoNeuronas.setBounds(16, 44, 89, 24);
		getContentPane().add(lblNoNeuronas);
		
		txtNeuronas = new JTextField(General.NUM_NEURONAS+"");
		txtNeuronas.setColumns(10);
		txtNeuronas.setBounds(115, 46, 161, 20);
		getContentPane().add(txtNeuronas);
		
		JLabel lblNoResultados = new JLabel("No. Resultados");
		lblNoResultados.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNoResultados.setHorizontalTextPosition(SwingConstants.RIGHT);
		lblNoResultados.setBounds(323, 43, 89, 24);
		getContentPane().add(lblNoResultados);
		
		txtResultados = new JTextField(General.CANT_SALIDAS+"");
		txtResultados.setColumns(10);
		txtResultados.setBounds(422, 43, 161, 20);
		getContentPane().add(txtResultados);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(486, 189, 97, 23);
		getContentPane().add(btnGuardar);
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					General.CAPA_ENTRADA = txtEntrada.getText();
					General.CAPA_OCULTA =  txtOculta.getText();
					General.CAPA_SALIDA = txtSalida.getText();
					General.NUM_NEURONAS = Integer.parseInt(txtNeuronas.getText());
					General.CANT_SALIDAS = Integer.parseInt(txtResultados.getText());
					General.URL_FILE_XML = txtFileXML.getText();
					JOptionPane.showMessageDialog(null, "Se se guardado correctamente.");
	            System.out.println(General.URL_FILE_XML);
				}catch(Exception ex){
					JOptionPane.showMessageDialog(null, "Ingrese valores v�lidos...");
				}
			}
		});

	}
	
	private void buscar_xml(){
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            txtFileXML.setText(file.toString());
            
        } else {
            System.out.println("Operacion Cancelada");
        }
	}	
}
