package presentation;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.border.TitledBorder;

import logic.Pixeles;
import logic.Valores;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JSplitPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmReconocimiento extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JLabel lblImgenesPredefinidas = new JLabel("Im\u00E1genes predefinidas             ");
	private JTextField txtURLImagen;
	private JLabel lblImagenShow;
	private JEditorPane panelEdicion;
	private int valorImagen=-1;
	private JScrollPane scrollPane;
	/**
	 * Create the frame.
	 */
	public FrmReconocimiento() {
		setTitle("Reconocimiento de imagen");
		setMaximizable(true);
		setClosable(true);
		setBounds(100, 100, 706, 428);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.EAST);
		panel.setLayout(new BorderLayout(0, 0));
		panel.add(lblImgenesPredefinidas, BorderLayout.NORTH);
		
		scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);
		
		scrollPane.setViewportView(new TestPane());
		
		JPanel panel_2 = new JPanel();
		getContentPane().add(panel_2, BorderLayout.CENTER);
		
		JLabel lblImagen = new JLabel("Imagen");
		
		txtURLImagen = new JTextField();
		txtURLImagen.setColumns(10);
		
		JButton btnBuscarImagen = new JButton("...");
		btnBuscarImagen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscar_imagen();
			}
		});
		
		JButton btnBuscar = new JButton("Buscar...");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iniciar_busqueda();
			}
		});
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		splitPane.setContinuousLayout(true);
		splitPane.setAutoscrolls(true);
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerSize(10);
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(splitPane, GroupLayout.DEFAULT_SIZE, 522, Short.MAX_VALUE)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblImagen, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(txtURLImagen, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnBuscarImagen)
							.addPreferredGap(ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
							.addComponent(btnBuscar, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblImagen)
						.addComponent(txtURLImagen, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnBuscarImagen)
						.addComponent(btnBuscar, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(splitPane, GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		lblImagenShow = new JLabel("Seleccione imagen ...");
		lblImagenShow.setHorizontalAlignment(SwingConstants.CENTER);
		splitPane.setLeftComponent(lblImagenShow);
		
		JPanel panel_3 = new JPanel();
		splitPane.setRightComponent(panel_3);
		panel_3.setSize(100,100);
		panel_3.setBorder(new TitledBorder(null, "Detalle de consola", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_3.add(scrollPane_1, BorderLayout.CENTER);
		
		panelEdicion = new JEditorPane();
		panelEdicion.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panelEdicion.setEditable(false);
		scrollPane_1.setViewportView(panelEdicion);
		splitPane.setDividerLocation(200);
		panel_2.setLayout(gl_panel_2);
	}
	
	private void buscar_imagen(){
		JFileChooser f = new JFileChooser();
	    f.setFileSelectionMode(JFileChooser.FILES_ONLY); 
	    f.showSaveDialog(null);
	    try{
	    	txtURLImagen.setText(f.getSelectedFile().toString());
	    	String url = txtURLImagen.getText();
	    	if(url.endsWith(".png") || url.endsWith(".jpg")){
	    		ImageIcon iconLogo = new ImageIcon(url);
	    		lblImagenShow.setIcon(iconLogo);
	    		lblImagenShow.setText("");
	    		System.out.println("LOL");
	    	}
	    	else
	    		lblImagenShow.setText("Seleccione la imagen...");
    		
	        //URL_DIRECTORY = txtDirectorio.getText();
	        //System.out.println("Hola: " + URL_DIRECTORY);
	    }catch(Exception e){
	    	System.out.println(e.getMessage());
	    }
	}
	
	private void iniciar_busqueda(){
		if(!txtURLImagen.getText().endsWith(".jpg")&&!txtURLImagen.getText().endsWith(".png")){
			JOptionPane.showMessageDialog(null, "No es un archivo v�lido para la b�squeda...");
			return;			
		}
		String URL_IMAGE = txtURLImagen.getText();
		System.out.println(URL_IMAGE);
		Pixeles px = new Pixeles(URL_IMAGE);
		px.generar();
		Double[][] matriz = px.getMatriz();
		for(int a = 0; a < matriz.length; a++){
			for(int b=0; b<matriz[a].length;b++){
				System.out.print(matriz[a][b]+"\t");
			}
			System.out.println();                                                                                                                                                      
		}
		System.out.println("cantidad de filas: " + matriz.length);
		System.out.println("cantidad de colum: " + matriz[0].length);
		
			/*
		
			Double matriz[][] = {{1.0,1.0,1.0,1.0,1.0},
								 {0.0,0.0,1.0,0.0,0.0},
								 {0.0,0.0,1.0,0.0,0.0},
								 {0.0,0.0,1.0,0.0,0.0},
								 {0.0,0.0,1.0,0.0,0.0},
								 {1.0,1.0,1.0,1.0,1.0}};*/
			//row, col, entrada, archivo, capa1, capa2, capa3, num_neuronas, num_respuestas
			Valores cl = new Valores(matriz.length, matriz[0].length, matriz, General.URL_FILE_XML, 
					General.CAPA_ENTRADA, General.CAPA_OCULTA, General.CAPA_SALIDA, General.NUM_NEURONAS, General.CANT_SALIDAS);
			System.out.println("Numero de neuronas: " + General.NUM_NEURONAS);
			System.out.println("Salidas: " + General.CANT_SALIDAS);
			

			
			String for_titulo = "*****************************************************************\n"+
								"\t MI_TITULO \n"										             +
								"******************************************************************\n";
			String valor = "";
			panelEdicion.setText("... Mostrando Resultados ....\n");
			//panelEdicion.setText(panelEdicion.getText() +  "Adios");
			
			cl.convertir_matriz();
			panelEdicion.setText(panelEdicion.getText() +  for_titulo.replaceAll("MI_TITULO", "SALIDA"));

			System.out.println("Salida-------------->");
			System.out.println("Sumatoria\t\tBias Capa Salida\t\tFuncion Simoidal");
			valor="Sumatoria\t\tBias Capa Salida\t\tFuncion Simoidal\n";
			for(int f =0; f <cl.salida_res.length; f++){
				for(int c=0; c < cl.salida_res[0].length;c++){
					valor+=cl.salida_res[f][c]+"\t";
					System.out.print(cl.salida_res[f][c]+"\t");
				}
				valor +="\n";
				System.out.println();
			}
			panelEdicion.setText(panelEdicion.getText() + valor);
			
			panelEdicion.setText(panelEdicion.getText() +  for_titulo.replaceAll("MI_TITULO", "RESULTADO"));
			System.out.println("Resultado-------------->");
			String []vocales = {"IMAGEN 1", "IMAGEN 2", "IMAGEN 3", "IMAGEN 4", "IMAGEN 5", "IMAGEN 6",
					"IMAGEN 7", "IMAGEN 8", "IMAGEN 9", "IMAGEN 10"};
			valor ="";
			for(int f =0; f <cl.salida_res.length; f++){
				double val =  Math.round(cl.salida_res[f][2]);
				valor += vocales[f] +"\t" +val+"\n";
				System.out.println(vocales[f] +"\t" + Math.round(cl.salida_res[f][2]));
				if(val == 1){
					valorImagen = f;
					scrollPane.setViewportView(new TestPane());
				}
			}
			panelEdicion.setText(panelEdicion.getText() +  valor);
	}
	
	public class TestPane extends JPanel {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public TestPane() {
            setLayout(new GridLayout(0, 1));
            File[] files = new File(General.URL_DIRECTORY_IMG).listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    String name = pathname.getName().toLowerCase();
                    System.out.println(name);
                    return pathname.isFile() &&
                            name.endsWith(".png") ||
                            name.endsWith(".jpg");
                }
            });
            Arrays.sort(files);
            int count = 0;
            while (count < files.length) {
                try {
                    System.out.println(count + "; " + files[count]);
                    
                    ImageIcon imageIcon = new ImageIcon(ImageIO.read(files[count])); // load the image to a imageIcon
                    Image image = imageIcon.getImage(); // transform it 
                    Image newimg = image.getScaledInstance(150, 150,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
                    imageIcon = new ImageIcon(newimg);  // transform it back
                    
                    JLabel lbl = new JLabel(imageIcon);
                    if(count == valorImagen){
                    	lbl = new JLabel("Seleccionada",imageIcon,  SwingConstants.CENTER);
                    	lbl.setForeground(Color.WHITE);
                    	lbl.setFont(new Font("Arial", Font.BOLD,14));
                    	lbl.setVerticalTextPosition(JLabel.CENTER);
                        lbl.setHorizontalTextPosition(JLabel.CENTER);
                        //lbl.setBorder(BorderFactory.createLineBorder(Color.black));
                    }
                    add(lbl);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                count++;
            }
            valorImagen =-1;
        }
    }
}
