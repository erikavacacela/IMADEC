package presentation;

import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {
     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		public ImagePanel(String URL_DIRECTORY) {
            setLayout(new GridLayout(0, 5, 5, 5));
            File[] files = new File(URL_DIRECTORY).listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    String name = pathname.getName().toLowerCase();
                    System.out.println(name);
                    return pathname.isFile() &&
                            name.endsWith(".png") ||
                            name.endsWith(".jpg");
                }
            });
            Arrays.sort(files);
            int count = 0;
            while (count < 6 && count < files.length) {
                try {
                    System.out.println(count + "; " + files[count]);
                    
                    ImageIcon imageIcon = new ImageIcon(ImageIO.read(files[count])); // load the image to a imageIcon
                    Image image = imageIcon.getImage(); // transform it 
                    Image newimg = image.getScaledInstance(150, 150,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
                    imageIcon = new ImageIcon(newimg);  // transform it back
                    
                    JLabel lbl = new JLabel(imageIcon);
                    add(lbl);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                count++;
            }
        }
}
	
