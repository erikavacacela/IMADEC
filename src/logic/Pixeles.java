package logic;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

public class Pixeles {
	private String URL_IMAGEN = "";
	private Double matriz_result[][]=null;
	
	public Pixeles(String imagen)
	{
		this.URL_IMAGEN = imagen;
	}
	
	public Double[][] getMatriz(){
		return matriz_result;
	} 
	
	public void generar()
	{
		try {
    		InputStream input = new FileInputStream(URL_IMAGEN);
	    	ImageInputStream imageInput = ImageIO.createImageInputStream(input);
	    	BufferedImage imagenL = ImageIO.read(imageInput);
	    	//File output = new File("C:/Users/jeffe/Desktop/u.txt"); //archivo de salida
	    	//BufferedWriter bw = new BufferedWriter(new FileWriter(output));
	    	int alto = imagenL.getHeight();
	    	int ancho=imagenL.getWidth();

	    	System.out.println("Alto: "+alto+"-- Ancho: "+ancho);

	    	matriz_result =  new Double[20][20];
	    	int fil=0, col=0;
	    	for (int y=0; y<alto;y+=10){
	    		fil=0;
	    		for(int x=0;x<ancho; x+=10){
	    			
	    			int srcPixel=imagenL.getRGB(x, y);
	    			Color c= new Color (srcPixel);
	    			int valR = c.getRed();
	    			int valG = c.getGreen();
	    			int valB = c.getBlue();
	    			if(valR<255 && valG<255 && valB<255){
	    				matriz_result[col][fil]=1.0;
	    			}else{
	    				matriz_result[col][fil]=0.0;
	    			}
	    			fil++;
	    			
	    		}
	    		col++;
	    	}
	    	/*
	    	matriz_result =  new Double[alto][ancho];
	    	for (int y=0; y<alto;y++){
	    		for(int x=0;x<ancho; x++){
	    			
	    			int srcPixel=imagenL.getRGB(x, y);
	    			Color c= new Color (srcPixel);
	    			int valR = c.getRed();
	    			int valG = c.getGreen();
	    			int valB = c.getBlue();
	    			if(valR<255 && valG<255 && valB<255){
	    				matriz_result[y][x]=1.0;
	    			}else{
	    				matriz_result[y][x]=0.0;
	    			}
	    			
	    		}
	    	}
	    	*/
	    	
	    	
	    	}catch(Exception e){
	    		System.out.println(e.toString());
	    	}
	}
}
