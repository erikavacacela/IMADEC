package logic;

import java.util.ArrayList;
import java.util.List;

public class Valores {
	Listas listas = null;
	int col = 5, row = 6;
	Double entrada[][] = null;
	public Double bias_capa_ent[][] = null;
	public Double transfercia[][] = null;
	public Double p_sinapsis_OS[][] = null; //Sinapsis de la capa oculta con la de salida
	public Double neuronas_res[][]=null;
	public Double salida_res[][]=null;
	int num_neuronas = 4, num_posibilidades = 5;
	public List<Peso> pesos_neuronas = null; 
	
	
	public Valores(int row, int col, Double[][] entrada, String archivo, String capa1, String capa2, String capa3,
			int num_neuronas, int num_respuestas){
		this.col=col; this.row=row;
		this.num_neuronas = num_neuronas; 
		this.num_posibilidades = num_respuestas;
		this.entrada = entrada;
		this.listas = new Listas(archivo, capa1, capa2, capa3);
		
		bias_capa_ent = new Double[this.row][this.col];
		transfercia = new Double[this.row][this.col];
		p_sinapsis_OS = new Double[this.num_posibilidades][this.num_neuronas];
		neuronas_res = new Double[this.num_posibilidades][this.num_neuronas];
		salida_res = new Double[this.num_posibilidades][3];
		pesos_neuronas = new ArrayList<Peso>();
	}
	
	public void convertir_matriz(){
		int cont = 0;
		for(int f = 0; f <this.row; f++){
			for(int c=0; c<this.col; c++){
				bias_capa_ent[f][c] = listas.getFistLayer().get(cont);
				//System.out.print(bias_capa_ent[f][c] + "\t");
				cont ++;
			}
			//System.out.println();
		}
		//System.out.println();
		//System.out.println("Transferencia");
		cont = 0;
		for(int f = 0; f <this.row; f++){
			for(int c=0; c<this.col; c++){
				Double WI = bias_capa_ent[f][c];
				Double XI = entrada[f][c];
				
				transfercia[f][c] = 1/(1+(Math.exp(-(WI+XI))));
				//System.out.println(WI + "<==>" + XI + " == " + transfercia[f][c]);
				//System.out.print(transfercia[f][c] + "\t");
				cont ++;
			}
			//System.out.println();
		}
		
		System.out.println("Pesos");
		Double p_neurona[][] = null;
		int contador = 0; 
		for(int n=0; n<num_neuronas; n++){
			//System.out.println("Valor -->");
			cont = 0;
			p_neurona = new Double[row][col];
			for(int f = 0; f <this.row; f++){
				for(int c=0; c<this.col; c++){
					p_neurona[f][c]= listas.getSynapse2().get(contador);
					//System.out.print(p_neurona[f][c] + "\t");
					cont ++;
					contador+=num_neuronas;
					if(contador>=listas.getSynapse2().size())
						contador = n+1;
				}
				//System.out.println();
			}
			
		
			Peso p = new Peso(p_neurona, listas.getSecondLayer().get(n));
			/*/System.out.println(listas.getSecondLayer().get(n) + " * " + p.suma_lateral +"\t=>"+p.getRespuesta());
			System.out.println();
			System.out.println();
			Double numero = 0.0;
			for(int i = 0; i< p.ma_lateral.length; i++){
				Double fila = 0.0;
				for(int j=0; j<p.ma_lateral[0].length; j++){
					numero = p.ma_lateral[i][j] + numero;
					fila = p.ma_lateral[i][j]+fila;
					System.out.print(p.ma_lateral[i][j]+"\t");
				}
				System.out.println("Suma fila : " + fila);
			}
			System.out.println("Suma: " +numero);*/
			pesos_neuronas.add(p);
		}
		
		//System.out.println("Pesos Sinapsis 2");
		Double aux_p[][] = new Double[this.num_neuronas][this.num_posibilidades];
		
		contador =0;
		for(int f=0; f<num_neuronas; f++){
			for(int c=0; c<num_posibilidades; c++){
				aux_p[f][c]=listas.getSynapse3().get(contador);
				//System.out.print(aux_p[f][c]+"\t");
				contador++;
			}
			//System.out.println();
		}
		//System.out.println();
		contador =0;
		for(int f=0; f<num_posibilidades; f++){
			for(int c=0; c<num_neuronas; c++){
				p_sinapsis_OS[f][c]=aux_p[c][f];
				//System.out.print(p_sinapsis_OS[f][c]+"\t");
				contador++;
			}
			//System.out.println();
		}
		
		
		System.out.println("Neuronas ...");
		contador =0;
		for(int f=0; f<num_posibilidades; f++){
			for(int c=0; c<num_neuronas; c++){
				neuronas_res[f][c]= p_sinapsis_OS[f][c] * pesos_neuronas.get(c).getRespuesta();
				System.out.print(p_sinapsis_OS[f][c]+"\t" + pesos_neuronas.get(c).getRespuesta());
				contador++;
			}	
			System.out.println();
		}
		
		System.out.println("Salida...");
		for(int f=0; f<num_posibilidades; f++){
			for(int c=0; c<3; c++){
				if(c == 0){
					Double suma = 0.0;
					for(int n=0; n<num_neuronas; n++){
						suma += neuronas_res[f][n];
					}
					salida_res[f][c]= suma;
				}else if(c==1){
					salida_res[f][c]=listas.getThirdLayer().get(f);
				}else{
					salida_res[f][c]=1/(1+(Math.exp(-(salida_res[f][0]+salida_res[f][1]))));
				}
				System.out.print(salida_res[f][c]+"\t");
			}	
			System.out.println();
		}
	}
	
	public class Peso{
		public Double p_neurona[][] = null;
		public Double ma_lateral[][] = null;
		public Double suma_lateral=0.0;
		public Double bia_capa_oculta=0.0;
		private Double respuesta=0.0;
		
		public Peso(Double[][] p_neurona, Double bia_co ){
			this.p_neurona = p_neurona;
			this.bia_capa_oculta = bia_co;
			this.ma_lateral = new Double[row][col];
			calcularMatrizLateral();
			calcularSumaLateral();
		}
		
		private void calcularMatrizLateral(){
			for(int f = 0; f <row; f++){
				for(int c=0; c<col; c++){
					Double valor1 = this.p_neurona[f][c];
					Double valor2 = transfercia[f][c];
					
					this.ma_lateral[f][c] = valor1 * valor2;
				}
			}
		}
		
		public Double calcularSumaLateral(){
			this.suma_lateral = 0.0;
			for(int f = 0; f <row; f++){
				for(int c=0; c<col; c++){
					this.suma_lateral +=ma_lateral[f][c];
				}
			}
			return this.suma_lateral;
		}
		
		public Double getRespuesta()
		{
			this.respuesta = 1/(1+(Math.exp(-(this.suma_lateral + this.bia_capa_oculta))));
			return this.respuesta;
		}
		
	}
	
}
