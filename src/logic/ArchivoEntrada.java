package logic;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import presentation.General;

public class ArchivoEntrada {
	
	public ArchivoEntrada(){
				
	}
	
	public String generar_archivo()
	{
		try{
			File[] files = new File(General.URL_DIRECTORY_IMG).listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    String name = pathname.getName().toLowerCase();
                    //System.out.println(name);
                    return pathname.isFile() &&
                            name.endsWith(".png") ||
                            name.endsWith(".jpg");
                }
            });
            Arrays.sort(files);
            int count = 0, iniciar=1;
            File output = new File(General.URL_INPUT_FILE); //archivo de salida
	    	BufferedWriter bw = new BufferedWriter(new FileWriter(output));
	    	
            while (count < files.length) {
            	iniciar = 1;
                try {
                	String nombre_imagen = files[count].getPath();
                	System.out.println(files[count].getPath());
                	InputStream input = new FileInputStream(nombre_imagen);
        	    	ImageInputStream imageInput = ImageIO.createImageInputStream(input);
        	    	BufferedImage imagenL = ImageIO.read(imageInput);
        	    	
        	    	int alto = imagenL.getHeight();
        	    	int ancho=imagenL.getWidth();
        	    	
        	    	String value_binary ="";
        	    	int contador = 0;
        	    	//matriz_result =  new Double[alto][ancho];
        	    	for (int y=0; y<alto;y+=10){
        	    		
        	    		for(int x=0;x<ancho; x+=10){
        	    			if(x==0 && y==0)
        	    				iniciar=0;
        	    			else
        	    				iniciar=1;
        	    				
        	    			int srcPixel=imagenL.getRGB(x, y);

        	    			Color c= new Color (srcPixel);

        	    			int valR = c.getRed();
        	    			int valG = c.getGreen();
        	    			int valB = c.getBlue();
        	    			if(valR<255 && valG<255 && valB<255){
        	    				if(iniciar == 0)
        	    					value_binary += "1";
        	    				else
        	    					value_binary += ";1";	    					
        	    				contador++;
        	    				//matriz_result[y][x]=1.0;
        	    			}else{
        	    				if(iniciar == 0)
        	    					value_binary += "0";
        	    				else
        	    					value_binary += ";0";	    					
        	    				//matriz_result[y][x]=0.0;
        	    				contador++;
        	    			}
        	    		}
        	    	}
        	    	String resultado="";
        	    	for(int a=0; a<files.length; a++){
        	    		if(a==count)
        	    			resultado +=";1";
        	    		else
        	    			resultado +=";0";
        	    	}
        	    	value_binary+=resultado;
        	    	System.out.println(resultado);
        	    	bw.write(value_binary);
        	    	if(count < files.length-1)
        	    		bw.write("\n");        	    		
        			System.out.println(">> Contador: " + contador + "--" + value_binary.length());
    	    		System.out.println(">> Alto: " + alto + " -- Ancho: " + ancho);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                count++;
            }
	    	bw.close();
	    	return output.getPath();
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return null;
		}
	}
}
