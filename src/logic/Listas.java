package logic;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

public class Listas {
	private List<Double> firstLay = null;
	private List<Double> secondLay = null;
	private List<Double> thirdLay = null;
	private List<Double> synape2 = null;
	private List<Double> synape3 = null;
	private String archivo ="C:/Users/Erika/Desktop/TareasIA/moya.txt";
	private String nombreCapa1="",  nombreCapa2="", nombreCapa3="";
	
	public Listas(String archivo, String capa1, String capa2, String capa3)
	{
		firstLay = new ArrayList<Double>();
		secondLay = new ArrayList<Double>();
		thirdLay = new ArrayList<Double>();
		synape2 = new ArrayList<Double>();
		synape3 = new ArrayList<Double>();		
		this.archivo = archivo;
		this.nombreCapa1 = capa1;
		this.nombreCapa2 = capa2;
		this.nombreCapa3 = capa3;
		passFileToLists();
	}
	
	private void passFileToLists() {
		try {
	         File inputFile = new File(archivo);
	         SAXReader reader = new SAXReader();
	         Document document = reader.read( inputFile );

	         //System.out.println("Root element :"  + document.getRootElement().getName());

	         //Element classElement = document.getRootElement();
	         
	         List<Node> nodes = document.selectNodes("/org.joone.net.NeuralNet/org.joone.net.NeuralNet/default/layers/org.joone.engine.SigmoidLayer/org.joone.engine.Layer/default" );
	         double valor =0;
	         for (Node node : nodes) {
	            //System.out.println("\nCurrent Element :" + node.getName());
	            if(node.selectSingleNode("LayerName").getText().equals(nombreCapa1)){//CapaEntrada
	            	Node my_node = node.selectSingleNode("bias");
	            	List<Node> my_nodes = my_node.selectNodes("./org.joone.engine.Matrix/default/value/double-array/double");
	            	//int c=0;
	            	for (Node node_aux : my_nodes) {
	            		valor = Double.parseDouble(node_aux.getStringValue());
	            		firstLay.add(valor);
	            		//System.out.println(c+"=>"+valor);
	            		//c++;
	            	}
	            	//System.out.println("Funcion 1-2");
	            	Node my_output = node.selectSingleNode("outputPatternListeners");
	            	List<Node> my_values = my_output.selectNodes("./org.joone.engine.FullSynapse/array/org.joone.engine.Matrix/default/value/double-array/double");
	            	int cont=1;
	            	for (Node node_aux : my_values) {
	            		valor = Double.parseDouble(node_aux.getStringValue());
            			synape2.add(valor);
            			//System.out.println(cont + ":=)" + valor);
            			cont++;
	            	}	            	
	            }else if (node.selectSingleNode("LayerName").getText().equals(nombreCapa2)){
	            	Node my_node = node.selectSingleNode("bias");
	            	List<Node> my_nodes = my_node.selectNodes("./org.joone.engine.Matrix/default/value/double-array/double");
	            	//System.out.println("SECOND LAY");
	            	for (Node node_aux : my_nodes) {
	            		valor = Double.parseDouble(node_aux.getStringValue());
	            		secondLay.add(valor);
	            		//System.out.println(valor);
	            	}
	            	
	            	//System.out.println("LALA");
	            	Node my_output = node.selectSingleNode("outputPatternListeners");
	            	List<Node> my_values = my_output.selectNodes("./org.joone.engine.FullSynapse/array/org.joone.engine.Matrix/default/value/double-array/double");
	            	int cont=1;
	            	for (Node node_aux : my_values) {
	            		valor = Double.parseDouble(node_aux.getStringValue());
            			synape3.add(valor);
            			//System.out.println(cont + ":)" + valor);
            			cont++;
	            	}
	            }else if(node.selectSingleNode("LayerName").getText().equals(nombreCapa3)){
	            	//System.out.println("TERCERA");
	            	Node my_node = node.selectSingleNode("bias");
	            	List<Node> my_nodes = my_node.selectNodes("./org.joone.engine.Matrix/default/value/double-array/double");
	            	for (Node node_aux : my_nodes) {
	            		valor = Double.parseDouble(node_aux.getStringValue());
	            		thirdLay.add(valor);
	            		//System.out.println(valor);
	            	}
	            }
	         }
	      } catch (DocumentException e) {
	         e.printStackTrace();
	      }
	}

	public List<Double> getFistLayer()
	{
		return firstLay;
	}
	
	public List<Double> getSecondLayer()
	{
		return secondLay;
	}
	
	public List<Double> getThirdLayer()
	{
		return thirdLay;
	}
	
	public List<Double> getSynapse2()
	{
		return synape2;
	}
	
	public List<Double> getSynapse3()
	{
		return synape3;
	}	

}
